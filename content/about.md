---
title: "About Us"
date: 2021-06-03T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

The OSPO Alliance was [launched in June 2021](https://newsroom.eclipse.org/news/announcements/leading-european-open-source-non-profit-organizations-announce-ospo-alliance) by European non profit organisations — OW2, Eclipse Foundation, OpenForum Europe, and Foundation for Public Code — and concerned individuals to promote an approach to excellence in open source software management. Together we created the OSPO Alliance — an open experience-sharing platform to facilitate discovery of tools and best practices and help define the state of the art in this domain. 

The OSPO Alliance aims to bring actionable guidance and solutions to all organisations willing to professionally manage the usage, contribution to and publication of open source software, regardless of their size, revenue model or whether public and private. In particular it will help organisations make the best out of the inexorable advance of open source software in their information systems and processes. We will facilitate the discovery and implementation of tools and best practices when engaging with open source software.

By professionalizing the management of open source software, the OSPO Alliance will make engaging with open source software less risky and more predictable. It will lower barriers to adoption of open source and will enable organisations to leverage it to enhance their digital sovereignty.

Interested in joining the OSPO Alliance? Sign our [statement of support](/docs/statement_of_support.pdf) to join our members and partners.
