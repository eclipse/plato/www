---
title: "GGI: InnerSource"
date: 2023-10-07T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

----

**Table of contents**

{{< table_of_contents >}}

----

# InnerSource

InnerSource is growing in popularity within companies today, as it offers an approach based on successful open source practices within organisations' development teams. However, doing InnerSource isn't just copy and pasting those practices. They must be adapted to companies' unique culture and internal organisation. Let's take a closer look at what InnerSource is and isn't, and what the associated challenges are.

## What is InnerSource?

The term was first used by Tim O'Reilly in 2000, stating that Innersourcing is "[…] _the use of open source development techniques within the corporation._"

According to [InnerSource Commons](https://innersourcecommons.org/learn/books/innersource-patterns/), the reference foundation on the topic, InnerSource is the "_use of open source principles and practices for software development within the confines of an organization._"

## Why InnerSource?

Still according to [InnerSource Commons](https://innersourcecommons.org/learn/books/innersource-patterns/), "_for companies building mostly closed source software, InnerSource can be a great tool to help break down silos, encourage and scale internal collaboration, accelerate new engineer on-boarding, and identify opportunities to contribute software back to the open source world._"

It is interesting to note that the benefits of InnerSource can impact various functions within a company, not just engineering. As a result, some companies have found concrete advantages in areas such as:

* Legal functions: accelerating the establishment of cross-functional collaborations through the use of a ready-to-use legal framework (InnerSource license).
* Human Resources: managing scarce skills through a central experienced core team responsible for pooling effort and expertise.

## The InnerSource Controversy

InnerSource is surrounded by common myths one can hear from detractors. While not true open source, it shows great potential benefits for organisation deploying such an approach internally. Here are some of these myths:

* [MYTH] InnerSource is done at the expense of open source (mainly outbound):
  * Software projects stay behind the company firewall.
  * Less external contributions to open source.
* [MYTH] Hijacking the spirit of open source vs. approaching it.
* [MYTH] No InnerSource project has ever become an open source project.
* [MYTH] The motive for doing InnerSource is that it is similar to open source. But in reality if a developer values it, then a direct open source contribution should always be preferred.

Here are some facts about InnerSource practicing that bust most of the previous myths:

* [FACT] InnerSource is a way to welcome mainly closed companies in to the open source.
* [FACT] While the majority of the open source contributions are made by volunteers, we can advertise the participation in the open source to engineers using this list of “perceived benefits”.
* [FACT] In some (or most?) cases, companies do not follow an orderly and controlled development practice and this (GGI) can be a way to help them manage this.
* [FACT] It will still require _a lot_ of work in the conversion of closed to open licensing.
* [FACT] There are indeed cases of InnerSource project being open sourced:
  * Twitter Bootstrap.
  * Kubernetes from Google.
  * Docker from dotCloud (previous name of Docker Inc.).
  * React Native.
* [FACT] Open source benefits from the increase in software engineers that get familiar with open source practices, since InnerSource ones are very similar.

## Who is doing it?

Many companies have launched InnerSource initiatives or ISPO (InnerSource Program Office), some of them for a long time, other one more recently. Here is a non-ehxaustive list, mainly focused on European companies:

* Banco de Santander ([source](https://patterns.innersourcecommons.org/p/innersource-portal))
* BBC ([source](https://www.youtube.com/watch?v=pEGMxe6xz-0))
* Bosch ([source](https://web.archive.org/web/20230429145619/https://www.bosch.com/research/know-how/open-and-inner-source/))
* Comcast ([source](https://www.youtube.com/watch?v=msD-8-yrGfs\&t=6s))
* Ericsson ([source](https://innersourcecommons.org/learn/books/adopting-innersource-principles-and-case-studies/))
* Engie ([source](https://github.com/customer-stories/engie))
* IBM ([source](https://resources.github.com/innersource/fundamentals/))
* Mercedes ([source](https://www.youtube.com/watch?v=hVcGABbmI4Y))
* Microsoft ([source](https://www.youtube.com/watch?v=eZdx5MQCLA4))
* Nike ([source](https://www.youtube.com/watch?v=srPG-Tq0HIs\&list=PLq-odUc2x7i-A0sOgr-5JJUs5wkgdiXuR\&index=46))
* Nokia ([source](https://www.nokia.com/thought-leadership/articles/openness/openness-drives-innovation/))
* SNCF Connect \& Tech ([source 1](https://twitter.com/FrancoisN0/status/1645356213712846853), [source 2](https://www.slideshare.net/FrancoisN0/opensource-innersource-pour-acclrer-les-dveloppements))
* Paypal ([source](https://innersourcecommons.org/fr/learn/books/getting-started-with-innersource/))
* Philips([source](https://medium.com/philips-technology-blog/how-philips-used-innersource-to-maintain-its-edge-in-innovation-38c481e6fa03))
* Renault ([source](https://www.youtube.com/watch?v=aCbv46TfanA))
* SAP ([source](https://community.sap.com/topics/open-source/publications))
* Siemens([source](https://jfrog.com/blog/creating-an-inner-source-hub-at-siemens))
* Société Générale([source](https://github.com/customer-stories/societe-generale))
* Thales ([source](https://www.youtube.com/watch?v=aCbv46TfanA))
* VeePee([source](https://about.gitlab.com/customers/veepee))

## InnerSource Commons, an essential reference

An active and vibrant community of InnerSource practitionners, that works according to Open Source principles, can be found at [InnerSource Commons](https://innersourcecommons.org). They provide a lot of useful resources to get you up to speed on the matter, including [patterns](https://innersourcecommons.org/learn/patterns/), a [learning path](https://innersourcecommons.org/learn/learning-path/) and small ebooks:

* [Getting Started with InnerSource](https://innersourcecommons.org/learn/books/getting-started-with-innersource/) by Andy Oram.
* [Understanding the InnerSource Checklist](https://innersourcecommons.org/learn/books/understanding-the-innersource-checklist/) by Silona Bonewald.

## InnerSource Governance Differences

InnerSource brings specific challenges that are not met in Open Source. Yet most of the organizations creating privative software are already dealing with them:

* Dedicated, company-specific license for Innersource projects (for large companies with multiple legal entities).
* The public nature of open source saves it from transfer pricing challenges. The private nature of InnerSource exposes corporations operating in different juridictions to profit shifting liabilities.
* Motivations for contributions are very different:
  * InnerSource has a smaller pool of possible contributors because it is limited to the organization.
  * Showing one's professional skills is a driver for contributing. InnerSource limits this impact to the organization boundaries only.
  * Contributing to improve society is another driver for contributions that is limited in InnerSource.
  * Motivation needs therefore a stronger effort and will rely more on rewards and assignments.
  * Dealing with perfectionism fears like the impostor syndrome is easier in InnerSource due to the limited visibility of the code.
* Workforce outsourcing is more frequent, which affects governance in several ways.
* Assessing enterprise adequacy is easier for InnerSource because it is developed in-house.
* Findability tends to become a problem. Indexing information is less of a priority for corporations. Public search engines like DuckDuckGo, Google or Bing do a  much better job that InnerSource cannot leverage.
* InnerSource is in a slightly better position to control export since it lives in-house.
* Boundary control of IP leaking as source code is needed.

InnerSource continues to evolve as more companies adopt its principles and share their experiences. A later version of this handbook will provide a curated list of the GGI's activities relevant to InnerSource practitionners.
