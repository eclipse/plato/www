---
title: "OSPO OnRamp"
date: 2021-11-21T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

<img src="/images/OspoOnRamp.png" alt="OSPO OnRamp" style="float: right; margin: 30px">

The OSPO OnRamp meeting series provides a low-threshold entry point for organisations that want to exchange and learn about the basics on how to set up an Open Source Program Office and get started into open source.

The 90 minute meeting is planned in a monthly cadence and will mainly consist of two parts.

In the first part an invited presenter will share experiences, lessons learned and other cool stuff. We plan to record this part of the meeting and to upload the recording later at the OSPO OnRamp website for later reference. This way we hope to gather and retain valuable information for the community.

In the second part of the meeting we would like to provide a trustful and protected environment where all participants can openly share and discuss their challenges, problems or other actual topics around establishing Open Source in their respective organisations. This part of the meeting will be held according to the [Chatham House Rules](https://en.wikipedia.org/wiki/Chatham_House_Rule#:~:text=When%20a%20meeting%2C%20or%20part,other%20participant%2C%20may%20be%20revealed.). Therefore there will be no recording of this part of the meeting.

🌐 [Link to meeting - click to join](https://bbb.opencloud.lu/rooms/flo-iof-4xr-orc/join)

## Next meeting(s)

Meetings are scheduled for every third Friday of every other the month from 10:30-12:00 CEST. 

----

* 📅 **Friday, March 21st**, 10:30-12:00 CET \
  🎙️ **Agustín Benito Bethencourt** — Ecosystem manager at [SCANOSS](https://www.scanoss.com/) and Ecosystem Lead at the [Software Transparency Foundation](https://www.softwaretransparency.org/). \
  📜 **[osskb.org](http://osskb.org/) or how to create complete SBOMs using open source tools**
  > The Software Transparency Foundation offers a free of charge service called
  osskb.org to upstream developers, open source projects and non-profits,
  researchers and professionals. By using a variety of open source Software
  Composition Analysis (SCA) tools that integrates osskb.org as backend, they
  can detect all the open source software within their software compositions. In
  addition, they can find out information about the license of those software
  components, detect potentially problematic snippets in their code… 
  > 
  > osskb.org provides access through an openAPI to OSS KB (knowledge base),
  created and maintained by SCANOSS, a key shareholder of the STFoundation.
  > 
  > The presentation will introduce Software Transparency Foundation, and will
  describe the osskb.org service, the knowledge base behind it, OSS KB, as well
  as the openAPI. It will also demonstrate how different tools integrate into
  this service and participants will see some of them in action.
  > 
  > Finally, the open source code involved in the service, published in GitHub,
  > will be shown and described, so participants can check it, download it, create
  > their own knowledge base or even contribute to it.

  You can import this specific session into your calendar with this [📅 ICS file](/resources/onramp/20250321_onramp_calendar.ics).

<br/><br/>

* 📅 **Friday, May 16th**, 10:30-12:00 CEST \
  🎙️ **Gijs Hillenius**, Open Source Programme Office at the [European Commission](https://interoperable-europe.ec.europa.eu/collection/ec-ospo). \
  📜 **The EU OSPO Network**

<br/><br/>

* 📅 **Friday, September 19th**, 10:30-12:00 CEST \
  🎙️ To be announced. \
  📜 To be announced.

----

For your convenience, we provide: 

- 🗓️ an online calendar. [Click this link](https://portal.ospo-alliance.org/nextcloud/apps/calendar/p/xbESpsrHCSoPpPmH) to open it.
- ⤵️ a way to add them dynamically to your own calendar app and stay up to date. Add to your calendar app an Internet calendar [using this link](https://portal.ospo-alliance.org/nextcloud/remote.php/dav/public-calendars/xbESpsrHCSoPpPmH?export).

## Previous meetings

You can find all available recordings and available content from the previous meetings [🔄 here](/onramp/past_meetings).

## Mailing list

All news, infos and updates will be shared on the OSPO OnRamp mailing list.

Please subscribe at <https://framalistes.org/sympa/info/ospo.onramp>.

## Meeting topics

We would love to see topic proposals for the meeting via the maillinglist. The following list can be seen as starting point, which we will extend based on the community input:

* How to select the "right" open source project for adoption and contributions.
* How to identity sustainable, secure open source projects with respect also to processes and documentation.
* How to attract/raise interest of the C-Level management for a dedicated Open Source strategy.
* How to ease the formal "entry barriers" for developers to join open source activities.
* How to engage with the Open Source community as a company.

We don’t plan to establish a formal voting process but confirming interest into proposed projects via the mailing list will help us to identify the most relevant topics for the community.

Initially we will reach out to the OSPO Alliance network to identify speakers, who are willing to share insights from their perspective, but mid- to long-term we hope that the community itself will identify the right speakers for upcoming meetings.

## Additional Resources

Resources coming from different Open Source and/or OSPO communities and Open Source professionals to help people advance in their open source journey.

Guides and books:

* [OSS Good Governance Handbook](https://ospo-alliance.org/ggi/)
* [Open Source Program Offices: The Primer on Organizational Structures, Roles and Responsibilities, and Challenges](https://www.linkedin.com/pulse/open-source-program-offices-primer-organizational-roles-haddad/)
