---
title: "OSPO Testimonials .. they walk the talk!"
date: 2024-02-22T:17:03:00-04:00
show_featured_footer: false
layout: "single"
---

[![SebL](/images/people/SebL.png)](www.linkedin.com/in/sébastien-lejeune-26290a15)

Hi, I am Sébastien Lejeune, **Open Source Advocate** at **Thales** working at the Group Technical Directorate. 

## Context 

**Thales** is a multinational group with french roots that designs, develops and manufactures electronical systems as well as devices and equipment for the aerospace, defence, transportation and security sectors. 

Open-source context: 
* Open Source Software & Hardware are strategic pillars for Thales, we want to foster collaborations between the actors of these communities through our GitHub organization: https://github.com/ThalesGroup
* We did set an OSPO up back in 2020 and we have put in place a unified process & tooling to help collaborators to contribute to existing open source project and to share Thales assets in open source
* Open Source is sponsored by the top management and clearly defined in the group strategy

## My pain points

1. **Raise Awareness**: I aimed to spread awareness about this global initiative throughout the organization.
2. **Middle Management Buy-In**: My goal was to persuade middle management to actively encourage their teams to participate.
3. **Scaling Up**: I sought to increase the number of contributors, both within Thales and externally.

## How I did it

I joined the **OSPO Alliance** in 2022 and became an active committer after few months.

I instanciated the GGI board into our internal Gitlab at Thales and it helped me to have an overview about all the activities I had already achieved but also a lot of relevant resources to legitimate Thales Open Source strategy and to share them with different involved people.

## Achievements since 2020
* Established an OSPO: We created an Open Source Program Office (OSPO) to oversee our open-source efforts.
* Streamlined Processes: We implemented tools and streamlined processes to facilitate easy asset publishing and contributions by all collaborators.
* Comprehensive Documentation: A clear and accessible documentation was developed and shared across Thales.
* Foundation Memberships: We expanded our affiliations with prominent open-source foundations, including Eclipse, Linux Foundation, CNCF, and OpenHardware Group.
* Active Participation: Our involvement extended to various open-source working groups, such as Systematic Open Source Hub, Eclipse projects, CNCF projects, and the Good Governance Initiative.
