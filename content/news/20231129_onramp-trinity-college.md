---
title: "Next OnRamp meeting on December 15th is about Case Studies at Trinity College"
date: 2023-11-29T19:00:00+01:00
layout: "news"
---

Another round in the academic world for our OnRamp series. In a couple of weeks, we will welcome John Whelan who's Director of the OSPO of the Trinity College of Dublin. He will showcase "Case Studies from the Frontiers of Knowledge Transfer in a European University" 

Save the date!

- 📅 Friday, December 15th,
- ⏰ 10:30-12:00 CET
- 🎙️ **John Whelan** — Director of OSPO, Trinity Innovation, Trinity College, University of Dublin.
- 📜 **Case Studies from the Frontiers of Knowledge Transfer in a European University**.

As usual, it is an online event open to all with no registration in a safe environment. Just come and grab a seat. Please connect to our 🌐 [BigBlueButton instance](https://bbb.opencloud.lu/rooms/flo-iof-4xr-orc/join) on the given date and time.

> Trinity is the first University in Europe to have an official OSPO. However, for many years we have supported open source in our Technology Transfer Office through
>
> - Spinning Out Campus Companies with OSS business models.
> - Dual Licensing of Government Funded Research.
> - Open Source Education and Training for Researchers.
>
> These will be illustrated through practical and real case studies over the years.

![OnRamp SpeakerCard](/news/img/202312_OSPO_OnRamp_SpeakerCard_John_Whelan.jpg)

The OSPO OnRamp meeting series provides an open, neutral and friendly forum, low-threshold entry point to exchange and learn about the basics of how to set up an Open Source Program Office and get started with open source. More on the [dedicated page](/onramp).
