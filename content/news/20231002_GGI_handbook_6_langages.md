---
title: "The “Open Source Good Governance” handbook now available in six languages!"
date: 2023-10-02T15:59:00+02:00
layout: "news"
---

**Thanks to the collaborative effort from the open source community, the
OSPO Alliance GGI Handbook is today’s most translated document to help
organisations building OSPOs.**

🇩🇪 🇵🇹 🇫🇷 🇮🇹 🇳🇱 🇪🇸

The members of the OSPO Alliance are proud to announce the availability of
the [*Good Governance Initiative Handbook*](https://ospo-alliance.org/ggi/)
in six languages (Dutch, English, French, German, Italian, Portuguese).
With this major achievement, the handbook reaches out a potential of
thousands of readers (472 millions of persons are speaking one of the
translated languages, including the Brazilians). The GGI methodology
to help implement corporate-wide open source policies, and set up an
OSPO “Open Source Program Office”, can now be easily exported and
used worldwide. 

Thanks to the open translation process applied, a large number of
contributors has already been involved and this number is expected to
grow significantly in the coming years. Based on the open source
collaborative management tool Weblate, this process enables translators
to benefit from an easy to use interface and tight version control
integration, along with quality checks. Other languages will come in the
future, including Chinese which has been started.

In parallel of translations, other developments are happening such as
the improvement of the implementation methodology, with the GGI
Automated Deployment feature. New governance rules allow today to manage
the initiative as an open source project. Several Task Forces are also
meeting regularly and addressing specific topics, such as the metrics
integrated to measure successes at each step, or the integration of
InnerSource in the methodology. 

The GGI methodology will be soon presented publicly in some major open
source conferences, in Italy ([RIOS Open Source Week](https://opensourceweek.it/),
October 3-5 in Roma and [SFScon](https://www.sfscon.it/programs/2023/),
Bolzano, November 10-11) and in Germany ([EclipseCon](https://www.eclipsecon.org/2023),
October 16-19, in Ludwisburg).

Please read 
[our Press Release](/news/resources/PR-2309-GGI-OSPOAlliance-Translations-v4.pdf)
for more details.