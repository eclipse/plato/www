---
title: "🌍️ OSPO Alliance presence at LibreOffice Conference and OpenSource October 2024 in Luxembourg"
date: 2024-09-10T10:00:00+01:00
layout: "news"
---

We're happy to share that our team will present **OSPO Alliance** and the latest from our actions with GGI at [LibreOffice Conference and OpenSource 2024](https://conference.libreoffice.org/2024/agenda) in Luxembourg:

- **Event Details**: The conference will be held from October 10-12, 2024, in Belval, Esch-sur-Alzette, Luxembourg, with a community open day on October 9.
- **Focus Areas**: The event will cover topics like project financing, software security, the Cyber Resilience Act's impact on Open Source projects, and the role of **Open Source Programme Offices (OSPOs)**.
- **Educational Aspect**: LibreOffice is also an educational project, aiming to teach contributions to large projects and promote Open Source tools in the educational sector.

Come and meet our speakers to share your challenges and learnings with OSPO & OSS Governance!
- [Boris Baldassari](https://www.linkedin.com/in/borisbaldassari/): Senior Software Engineer at Eclipse Foundation
- [Florent Zara](https://www.linkedin.com/in/florentzara/):  Open Source Services Team Lead at Eclipse Foundation 