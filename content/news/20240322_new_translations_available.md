---
title: "Updates to translations: Dutch, German, French, Spanish"
date: 2024-03-22T10:00:00+01:00
layout: "news"
---

We are thrilled to announce that a new language has been added to our set of translations: we now have a **Spanish translation** of our GGI Handbook!

* [**Download it here**](/dl/docs/ggi_handbook_v1.2_es.pdf)!

Another piece of good news - we also have [other great updates for the v1.2 of our translations](/ggi) of the GGI Handbook!

The following languages have been updated to the latest version v1.2 and are now available in our downloads:
* **Dutch** - [Download it here](/dl/docs/ggi_handbook_v1.2_nl.pdf).
* **German** - [Download it here](/dl/docs/ggi_handbook_v1.2_de.pdf).
* **French** - [Download it here](/dl/docs/ggi_handbook_v1.2_fr.pdf).

Special thanks go to our awesome contributors for these languages: you are amazing!

