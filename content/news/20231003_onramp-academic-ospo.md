---
title: "Next OnRamp meeting on October 20th is about OSPO in the Academic World"
date: 2023-10-02T11:00:00+02:00
layout: "news"
---

In October, our next OnRamp meeting will feature Anne-Marie Scott from the board of the Apereo Foundation. She’ll tell us about OSPOs in the Academic World.

It’s open to all with no registration in a safe environment. Just come and grab a seat.

- 📅 Date: Friday, **October 20th, 10:30-12:00 CEST**
- 🎙️ Speakers: **Anne-Marie Scott** — Board Chair, **Apereo Foundation**.
- 📜 Agenda: **OSPO in the Academic World**,

> The establishment of OSPOs in higher education has seen a steady increase over the last few years, especially in North America where the support they can offer for open research is increasingly being recognised and funded. However, open education has a much longer history in universities, of which open source software is just one part. Drawing on our experiences within the Apereo community and beyond, this talk will unpack the concept of the “academic OSPO” in that wider open context, and highlight the full breadth of activities an OSPO could support in the academy.

🌐 [Link to meeting - click to join](https://bbb.opencloud.lu/rooms/flo-iof-4xr-orc/join)


![OnRamp SpeakerCard](/news/img/202310_OSPO_OnRamp_SpeakerCard_AM-Scott.png)

The OSPO OnRamp meeting series provides an open, neutral and friendly forum, low-threshold entry point to exchange and learn about the basics of how to set up an Open Source Program Office and get started with open source. More on the [dedicated page](/onramp).

