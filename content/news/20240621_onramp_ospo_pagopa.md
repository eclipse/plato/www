---
title: "Next OnRamp meeting on June 21st: OSPO in the public sector"
date: 2024-06-05T13:00:00+02:00
layout: "news"
---

After a deep dive in the [French State OSPO](/onramp/past_meetings/) in May, Leonardo Favario will conclude the first part of our 2024 season of our OnRamp sessions before the summer break, with an exclusive overview of an OSPO, still in the public sector, with a use case from Italy this time.

Save the date!

- 📅 Friday, June 21st
- ⏰ 10:30-12:00 CET
- 🎙️ **Leonardo Favario** — Head of Open Source Program Office at [PagoPA S.p.A.](https://www.pagopa.it/).
- 📜 **OSPO in the public sector: opportunities, challenges and the road ahead**.

As usual, it is an online event open to all with no registration in a safe environment. Just come and grab a seat. Please connect to our 🌐 [BigBlueButton instance](https://bbb.opencloud.lu/rooms/flo-iof-4xr-orc/join) on the given date and time.

> Gain insights from the recently established Open Source Program Office of PagoPA, an Italian public company. This presentation focuses on the inception of the OSPO, a multidisciplinary team responsible for managing all the open source initiatives within the organization. Emphasis will be placed on the development of the company's open source strategy and its alignment with Italian national norms and guidelines. Additionally, the discussion will address current hurdles and outline future directions, providing a comprehensive overview of the challenges and opportunities that lie ahead for the entire Italian public ecosystem.

![OnRamp SpeakerCard](/news/img/202406_OSPO_OnRamp_SpeakerCard_Leonardo_Favario.png)

_The OSPO OnRamp meeting series provides an open, neutral and friendly forum, low-threshold entry point to exchange and learn about the basics of how to set up an Open Source Program Office and get started with open source. More on the [dedicated page](/onramp)._
