---
title: "ICYMI: OnRamp sessions replays of October and November"
date: 2023-11-20T21:20:05+02:00
layout: "news"
---

In Case You Missed It, we hosted two great OnRamp sessions in October and November. Let's delve into the key highlights and takeaways from these enlightening sessions.

<img src="/resources/onramp_20231020/anne-marie-scott-picture.jpg" alt="Anne-Marie Scott" style="float: left; margin: 15px; max-width: 15%; max-height: 15%">

In October, Anne-Marie Scott (Board Chair of the Apereo Foundation and a member of the Board of the Open Source Initiative) unpacked the concept of  “***Academic OSPO***” and highlighted the full breadth of activities an OSPO could support in a context where open source software is just one part. Open research, open education, open science… are also part of a truly multi-faceted landscape.

[View the slides](/resources/onramp_20231020/20231020_OSPO_OnRamp_OSPO_Academic_World.pdf) or [watch the video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1697790501307) to learn more.

---

<img src="/resources/onramp_20231117/boris-baldassari-picture.jpg" alt="Anne-Marie Scott" style="float: right; margin: 10px; max-width: 15%; max-height: 15%">

In November, Boris Baldassari — Chairperson of the OSPO Alliance & Open Source Expert at the Eclipse Foundation — unveiled the new features of the just-released **version 1.2 of the *Good Governance Initiative***. He covered in details the new updates and additions to this iteration of the handbook:

- Relationships between activities,
- InnerSource chapter
- and My GGI Board.

One year after the previous version of the handbook, significant progress has been achieved, providing a more sophisticated and accurate support to organisations of all types. You'll get everything from the [slides](/resources/onramp_20231117/20231117_OSPO_OnRamp_GGI-1.2.pdf) and the [replay](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1700211741467).

---

All our past sessions can be found on our [previous meetings page](/onramp/past_meetings) and the upcoming OnRamp sessions are [available here](/onramp).