---
title: "🎦 OSPO Alliance team at the core of the Business and Challenges track from Open Source Experience 2024 (#OSXP2024)"
date: 2025-02-20T10:00:00+01:00
layout: "single"
---

Check out videos from **Open Source Experience 2024 (#OSXP2024)** as the **OSPO Alliance team** had the pleasure to be at the core of the **Business and Challenges** conversations and track. Revisit or discover, share all topics, important food for thoughts:

## Unlocking the Strategic Value of Open Source in Business (Eng) ##
Open source software is a cornerstone of modern technology, driving innovation and efficiency. Despite its widespread adoption, challenges persist for open source advocates. This talk explores the strategic value of open source and addresses key hurdles faced by OSPO champions, including real world examples from companies and administrations.

**Speakers**
- Nicolas Toussaint - Orange Business
- Philippe Bareille - Open Source Program Officer - City of Paris
- Silona BONEWALD - IEEE SA OPEN
- Sébastien Lejeune - Open Source Advocate - Thales
- Simon Clavier - Responsable de la Stratégie Open Source - Groupe SNCF

**Replay**
- 🎦 https://www.youtube.com/watch?v=SpxtkfqkxVw&list=PLJjbbmRgu6RoPeMXvt_VSO7C8HCpgo6q2&index=22&pp=iAQB

## Happy birthday Thales OSPO ! (Fr) ##
Thales’ Open Source Program Office (OSPO) has reached a significant milestone: five years of existence. We will share our experiences, lessons learned, and challenges encountered during this period.

**Speaker**
- Sébastien Lejeune - Open Source Advocate - Thales

**Replay**
- 🎦 https://www.youtube.com/watch?v=VXRNtagK-kE&list=PLJjbbmRgu6RoPeMXvt_VSO7C8HCpgo6q2&index=21

## Competitive dynamics in open source (Fr) ##
Round table open discussion to question competition within the open source ecosystem. Whether it’s between companies or, more broadly, within communities or between contributors, stakes involved in this little-addressed yet essential topic for understanding your environment and positioning yourself as a player. 

**Speakers**
- Amel Charleux - Maître de Conférences - Université de Montpellier
- Laurent Marie - CEO - Worteks
- Pierre Baudracco - CEO - BlueMind
- Frédéric Aatz - Chair & Maintainer - OSPO Alliance

**Replay**
- 🎦 https://www.youtube.com/watch?v=HdZMS7f8vQw&list=PLJjbbmRgu6RoPeMXvt_VSO7C8HCpgo6q2&index=20

## Full Open Source Experience 2024 program and replays ##
![Open Source Experience 2024 Replays](/news/img/202501_OSXP_Replays.png)
- Program conference https://www.opensource-experience.com/en/event/
- 🎦 All replays from Open Source Experience 2024: https://www.youtube.com/playlist?list=PLJjbbmRgu6RqGMOhahm2iE6NUkIYIaEDK