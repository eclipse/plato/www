---
title: "🎉 2024 RetrOSPOctive!"
date: 2025-01-27T08:00:00+01:00
layout: "single"
---

Dear community,

We wish you all the best for this new year, may 2025 bring you and your dearest health and  happiness!

## 2024 Retr***OSPO***ctive

Looking back, 2024 has been a great year for our 3<sup>rd</sup> year of existence.

### 🎉 Achievements

- 🎓 5 [OnRamp sessions](https://ospo-alliance.org/onramp/past_meetings/)
- 🇵🇹 A New Translation for the [GGI Handbook](https://ospo-alliance.org/ggi/), bringing the total to 6 languages: 🇫🇷 🇩🇪 🇮🇹 🇪🇸 🇳🇱 🇵🇹
- ✨ 6 New [Supporting Members](https://ospo-alliance.org/membership/)
- 📄 2 New Sections on the OSPO Alliance website:
  - 🤝 [Trusted Services Partners](https://ospo-alliance.org/partners/) (🏢 2 Entities)
  - 📝 [OSPO Stories](https://ospo-alliance.org/ospo-stories/)

### 📊 Analytics
- 🐦 750 Followers across our social media channels
- 📨 100+ Organisations registered on our mailing list
- 🌐 5,400+ Visitors to our website

![2024 infography](/news/img/OSPO_Alliance_HNY2025.png)

## Looking Ahead: Plans for 2025

Moving forward, our intent for 2025 is to engage with you more effectively and bring more value to our community. To do so we plan to work on:
- **Content communication** twice a month (news, article, testimonials…),
- **Quarterly call** to comment on those topics followed by open discussions and feedbacks under Chatham house rules (2<sup>nd</sup> Monday of new quarter, 45 minutes at 16:00 CET),
- **Quarterly recap** email with new members, project updates, onramp recap, key metrics (2<sup>nd</sup> week new quarter).
