# ospo-alliance.org

Welcome to the [ospo-alliance.org](https://ospo-alliance.org) development place. This project is part of the larger OSPO Alliance initiative, and includes materials from the [Good Governance Initiative](https://gitlab.ow2.org/ggi/).

This project is community-led, and all contributions are welcome. The website itself is generated with [Hugo](https://gohugo.io/documentation/).

## Getting started

Please use our contributors mailing list before contributing.

### Required Software

| Software | Version |
|---------|---------|
| node.js | 18.13.0 |
| npm | 8.19 |
| Hugo | 0.110 |
| Git | > 2.31 |

Install dependencies (depends on your operating system)

- [Node.js](https://nodejs.org/) v18.13.0
- [Hugo](https://gohugo.io/) v0.110.0 ([binaries available here](https://github.com/gohugoio/hugo/releases/tag/v0.110.0))

See our [Managing Required Software](https://gitlab.eclipse.org/eclipsefdn/it/webdev/hugo-solstice-theme/-/wikis/Managing-Required-Software) wiki page for more information on this topic.

Build assets and start a webserver:

```bash
yarn
hugo server
```

## Contributing

1. [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html/) the [eclipse/plato/www](https://gitlab.eclipse.org/eclipse/plato/www) repository
2. Clone repository: `git clone https://gitlab.eclipse.org/[your_username]/www.git`
3. Create your feature branch: `git checkout -b my-new-feature`
4. Commit your changes: `git commit -m 'Add some feature' -s`
5. Push feature branch: `git push origin my-new-feature`
6. Submit a merge request

### Declared Project Licenses

This website and the accompanying materials are made available under the terms
of the Creative Commons Attribution 4.0 (International) License which is
available at https://creativecommons.org/licenses/by/4.0/.

SPDX-License-Identifier: CC-BY-4.0

## Related projects


### [Good Governance Initiative](https://gitlab.ow2.org/ggi/)

A set of guidelines and best practices to set up an Open Source Program Office (OSPO) within an organisation.

### [EclipseFdn/solstice-assets](https://github.com/EclipseFdn/solstice-assets)

Images, less and JavaScript files for the Eclipse Foundation look and feel.

### [EclipseFdn/hugo-solstice-theme](https://github.com/EclipseFdn/hugo-solstice-theme)

Hugo theme of the Eclipse Foundation look and feel.

## Bugs and feature requests

Have a bug or a feature request? Please search for existing and closed issues. If your problem or idea is not addressed yet, [please open a new issue](https://gitlab.eclipse.org/eclipse/plato/www/issues/new).

## Authors

**Christopher Guindon (Eclipse Foundation)**

- <https://twitter.com/chrisguindon>
- <https://github.com/chrisguindon>

## Trademarks

To be updated

## Copyright and license

Copyright (c) 2023 Eclipse Foundation, OW2, OpenForum Europe, Foundation for Public Code and others

This website and the accompanying materials are made available under the
terms of the Creative Commons Attribution 4.0 (International) License which
is available at https://creativecommons.org/licenses/by/4.0/.

SPDX-License-Identifier: CC-BY-4.0
